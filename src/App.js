import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Menu from './components/Menu';
import MainPage from './components/MainPage';
import Home from './components/Home';
import Video from './components/Video';
import NotFound from './components/NotFound';
import Account from './components/Account';
import Auth from './components/Auth';
import Welcome from './components/Welcome';
import Post from './components/Post';
import {ProtectedRoute} from './components/ProtectedRoute'
import './App.css';
function App() {
  return (
    <div>
         <Router>
           <Menu/>
              <Switch>
                 <Route path='/' exact component={MainPage}/>
                  <Route path='/Home' component={Home}/>
                  <Route path='/Post/:id' component={Post}/>
                  <Route path='/Video' component={Video}/>
                  <Route path='/Account' component={Account}/>
                  <Route path='/Auth' component={Auth}/>
                  <ProtectedRoute path='/Welcome' component={Welcome}/>
                  <Route path='*' component={NotFound}/>
              </Switch>
         </Router>
    </div>
  );
}

export default App;
