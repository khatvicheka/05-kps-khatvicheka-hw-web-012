import React from "react";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AccountNaame from "./AccountNaame";

function Account() {
  return (
    <div className="container">
      <Router>
        <ul>
          <li>
            <Link to="/Account?name=netflix">Netflix</Link>
          </li>
          <li>
            <Link to="/Account?name=Zillow Group">Zillow Group</Link>
          </li>
          <li>
            <Link to="/Account?name=Yahoo">Yahoo</Link>
          </li>
          <li>
            <Link to="/Account?name=Modus Create">Modus Create</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/Account"  component={AccountNaame} />
        </Switch>
      </Router>
    </div>
  );
}

export default Account;
