import React from "react";
import { Col, Form } from "react-bootstrap";
import Check from "./Check";

function Auth(props) {
  return (
    <div className="container">
      <Form>
        <Form.Row>
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
        </Form.Row>
        <button
          onClick={() => {
            Check.login(() => {
              props.history.push("/Welcome");
            });
          }}
        >
          Login
        </button>
      </Form>
    </div>
  );
}

export default Auth;
