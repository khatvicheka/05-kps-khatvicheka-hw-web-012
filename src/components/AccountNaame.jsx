import React from "react";
import queryString from "query-string";

function AccountNaame(props) {
  let {name} = queryString.parse(props.location.search);

  console.log(name);
  return (
    <div>
      {name ? (
        <h3>
          The <code>name</code> in the query string is &quot;{name}
          &quot;
        </h3>
      ) : (
        <h3>There is no name in the query string</h3>
      )}
    </div>
  );
}

export default AccountNaame;
