import React from "react";
import { Route, Redirect } from "react-router-dom";
import Check from "./Check";

export const ProtectedRoute = ({
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (Check.isAuthenticated()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/Auth",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};