import React, { Component } from "react";
import CardItem from "./CardItem";

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      Users: [
        {
          id: "1",
          name: "Dara",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://lh3.googleusercontent.com/proxy/yHB0VFKT3u21mB7xnlrTd2fG5VPhBhZyyt8dznYxvh7s_7CD8eaDe1QAKf4XoilXuQkOwxbUL4SsjBb3NnZajN0VDb4yFQ1ABh00AG0Cid5Ie7XYYe4S",
        },
        {
          id: "2",
          name: "Sarith",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://www.billiardport.com/assets/pages/media/profile/profile_user.jpg",
        },
        {
          id: "3",
          name: "Vannet",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://domain7.com/uploads/profiles/hover/_teamImage/everett-hover.jpg",
        },
        {
          id: "4",
          name: "Kanann",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://lh3.googleusercontent.com/proxy/IbyegHoIJ4E3UPro2jMh4UPO-o-jyGcR32z8ru9Y_PwY93ceLNrmEHVmnskJu6xBPFQSHsr3PVFzV3IqpP2aoAjx9RdF5LRFVVs75bRLK89Q_LA-d7idipWBzjs",
        },
        {
          id: "5",
          name: "Sopha",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://barefootmedia.co.uk/wp-content/uploads/2016/01/Chris-user-profile.jpg",
        },
        {
          id: "6",
          name: "Visal",
          des:
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
          img:
            "https://www.wrappixel.com/demos/admin-templates/agile-admin/plugins/images/users/1.jpg",
        },
      ],
    };
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.state.Users.map((item, index) => (
            <CardItem
              key={item.id}
              name={item.name}
              des={item.des}
              img={item.img}
              id={item.id}
            />
          ))}
        </div>
      </div>
    );
  }
}
