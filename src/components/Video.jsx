import React from "react";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Animation from "./Animation";
import Movie from "./Movie";

function Video() {
  return (
    <div className="container">
      <Router>
        <ul>
          <li>
            <Link to="/Video/Animation">Animation</Link>
          </li>
          <li>
            <Link to="/Video/Movie">Movie</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/Video/Animation" component={Animation} />
          <Route path='/Video/Movie' component={Movie}/>
             <h3>Please Select A Topic</h3>
        </Switch>
      </Router>
    </div>
  );
}

export default Video;
