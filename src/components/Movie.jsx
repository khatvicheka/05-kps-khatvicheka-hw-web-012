import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Action from "./Action";
function Movie() {
    return (
        <div>
            <Router>
                 <h3>Movie Category</h3>
                <ul>
                    <li>
                        <Link to="/Video/Movie/adventure">Adventure</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/comedy">Comedy</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/crime">Crime</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie/documentary">Documentary</Link>
                    </li>
                </ul>
                <Switch>
                <Route path="/Video/Movie/:name" component={Action} />
                <h3>Please Select A Topic</h3>
                </Switch>
            </Router>
        </div>
    )
}

export default Movie
