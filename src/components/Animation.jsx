import React from "react";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Action from "./Action";
function Animation() {
  return (
    <div>
      <Router>
        <h3>Animation Category</h3>
        <ul>
          <li>
            <Link to="/Video/Animation/action">Action</Link>
          </li>
          <li>
            <Link to="/Video/Animation/romance">Romance</Link>
          </li>
          <li>
            <Link to="/Video/Animation/comedy">Comedy</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/Video/Animation/:name" component={Action} />
          <h1>Please Select A Topic</h1>
        </Switch>
      </Router>
    </div>
  );
}
export default Animation;
